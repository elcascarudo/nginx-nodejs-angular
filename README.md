# Imagen para despliegues de aplicaciónes realizadas en Angular

### Aplicaciónes instaladas

 - Nginx
 - NodeJS
 - Angular CLI

### Directorio de trabajo

```docker
WORKDIR /var/www/html
```

### Notas

Se recomienda eliminar el archivo "index.nginx-debian.html" que viene por defecto en la instalacion de Nginx

Ejemplo de uso: [despliegue-angular-node](https://bitbucket.org/elcascarudo/despliegue-angular-node/src/master/ "despliegue-angular-node")